/* 
 * Hash Table kit - generic interface data for hash table implementations.
 * (C) 2012-2014 Francesco Romani - fromani at gmail dot com. ZLIB licensed.
 */

/** \file hashtablekit.h
    \brief the common hash table kit interface.
*/


#ifndef CX_HASHTABLEKIT_H
#define CX_HASHTABLEKIT_H

#include <stdio.h>

#include <stdint.h>
#include <inttypes.h>


/** \enum Implementation defaults for HASHTABLEKIT. */
enum {
    CX_HT_MINIMUM_POWER    = 5,
    CX_HT_MAXIMUM_POWER    = 31
};

/** \enum Error codes for the MHashTable functions. */
/** DO NOT rely on their values. ABI isn't guaranteed */
typedef enum {
    CX_HT_OK = 0,                   /**< so far, so good.                 */
    CX_HT_ERROR_GENERIC,            /**< you shouldn't get this.          */
    CX_HT_ERROR_BAD_SIGNATURE,      /**< provided an incompatible object. */
    CX_HT_ERROR_NO_MEMORY,          /**< out of memory.                   */
    CX_HT_ERROR_INTERNAL,           /**< corrupted MHash data (PANIC!)    */
    CX_HT_ERROR_NULL_REFERENCE,     /**< given a bad MHash reference.     */
    CX_HT_ERROR_BAD_ARGUMENTS,      /**< given wrong arguments.           */
    CX_HT_ERROR_KEY_NOT_FOUND,      /**< requested a missing key.         */
    CX_HT_ERROR_KEY_DUPLICATED,     /**< found a duplicated key.          */
} CX_HTError;

typedef struct CX_HashTable_ CX_HashTable;
struct CX_HashTable_ {
    uint32_t signature;
    void *implementation;

    uint32_t seed;
    uint32_t (*hash)(const void *data, int len, uint32_t seed);

    /**
      \fn Disposes a CX_HashTable.
      \param ht a CX_HashTable reference.
      \return 0 on success, a CX_HTError otherwise.
      \see mccfht_new
    */
    int (*del)(CX_HashTable *ht);
    /**
      \fn Adds a new element to the table, aborting on collision.
      This function will fail if the key is already present in the table.
      NOTICE: this functions DOESN'T COPY the key data NOR the element data.
      The caller MUST TAKE CARE EXPLICITELY of managing the life time of
      both those objects.
      \param ht a CX_HashTable reference.
      \param key_data constant pointer to the key data for this element.
      \param key_size size in bytes of the key.
      \param data pointer to the data of the element to be added.
      \return 0 on success, a CX_HTError otherwise.
    */
    int (*add)(CX_HashTable *ht,
               const uint8_t *key_data, size_t key_size,
               const void *data);
    /**
      \fn Removes an element from the table.
      \param ht a CX_HashTable reference.
      \param key_data constant pointer to the key data for this element.
      \param key_size size in bytes of the key.
      \param data pointer to storage which will held a copy of the
             removed element. Can be NULL, in that case the old element
             is silently discarded.
      \return 0 on success, a CX_HTError otherwise.
    */
    int (*remove)(CX_HashTable *ht,
                  const uint8_t *key_data, size_t key_size,
                  const void **data);
    /**
      \fn Finds an element into the table.
      \param ht a CX_HashTable reference.
      \param key_data constant pointer to the key data for this element.
      \param key_size size in bytes of the key.
      \param data pointer to storage which will held a copy of the
             found element. Can be NULL, in that case the found element
             isn't copied at all.
      \return 0 on success (element found), a CX_HTError otherwise.
    */
    int (*find)(CX_HashTable *ht,
                const uint8_t *key_data, size_t key_size,
                const void **data);
};


#endif /* CX_HASHTABLEKIT_H */

