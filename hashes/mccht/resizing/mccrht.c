/* 
* MHashTable - a murmur-based, chanining, composite resizing hash table.
* (C) 2010-2014 Francesco Romani - fromani at gmail dot com. ZLIB licensed.
*/


#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "arraykit.h"
#include "hashlib.h"

#include "mhash.h"

/* FIXME: how to cope with shadowing? */
/* FIXME: resize-in-resize */


typedef struct {
    const uint8_t   *key_data;
    size_t          key_size;
    void            *data;
} MHashTableItem;


struct mhashtable_ {
    CX_VArray   **buckets;

    int         power;

    uint32_t    count;
    uint32_t    used;
    uint32_t    used_max;

    int         percent_load_factor;
    int         resize_items_amount;

    MHashTable  *shadow;
    uint32_t    moved_items;
};

enum {
    MH_DEFAULT_BUCKET_SIZE = 8,
    MH_SEED                = 0x00000023 /* FIXME */
};

typedef int (*MHashTableKeyCollisionHandler)(CX_VArray *bucket,
                                             MHashTableItem *current_item,
                                             MHashTableItem *new_item);


/* internal prototypes */
static uint32_t mh_size(uint32_t power);
static uint32_t mh_mask(uint32_t power);
static uint32_t mh_load(uint32_t items, int percent_load_factor);
static int mh_needs_resize(MHashTable *ht);
static int mh_resize_if_needed(MHashTable *ht);
static int mh_switch_tables(MHashTable *ht);
static int mh_are_keys_equal(const uint8_t *key_data1, size_t key_size1,
                             const uint8_t *key_data2, size_t key_size2);
static int mh_find_item_in_bucket(CX_VArray *bucket,
                                  const uint8_t *key_data,
                                  size_t key_size,
                                  MHashTableItem **item,
                                  size_t *idx);
static uint32_t mh_hash(MHashTable *ht,
                        const uint8_t *key_data, size_t key_size);
int mh_add_to_bucket(MHashTable *ht, CX_VArray *bucket,
                  const uint8_t *key_data, size_t key_size, void *data,
                  MHashTableKeyCollisionHandler on_collision);
static int mh_migrate_bucket(CX_VArray *old_bucket, MHashTable *new_ht);
static int mh_migration_step_done(MHashTable *ht, uint32_t idx);
static int mh_migrate_table_step(MHashTable *ht);
static CX_VArray *mh_get_bucket(MHashTable *ht, uint32_t idx);
int mh_add(MHashTable *ht,
           const uint8_t *key_data, size_t key_size, void *data,
           MHashTableKeyCollisionHandler on_collision);
static int mh_on_collision_error(CX_VArray *bucket,
                                 MHashTableItem *current_item,
                                 MHashTableItem *new_item);
static int mh_on_collision_replace(CX_VArray *bucket,
                                   MHashTableItem *current_item,
                                   MHashTableItem *new_item);
static CX_VArray *mh_find_bucket(MHashTable *ht,
                                 const uint8_t *key_data, size_t key_size);
static CX_VArray *mh_lookup_bucket(MHashTable *ht,
                                   const uint8_t *key_data, size_t key_size);



#define MH_CLAMP(X, LL, UL) do { \
    if ((X) < (LL)) { \
        (X) = (LL); \
    } \
    if ((X) > (UL)) { \
        (X) = (UL); \
    } \
} while (0)

#define MH_SET_PTR(PTR, VAL) do { \
    if ((PTR)) { \
        *(PTR) = (VAL); \
    } \
} while (0)

static uint32_t mh_size(uint32_t power)
{
    return (1 << power);
}

static uint32_t mh_mask(uint32_t power)
{
    return (mh_size(power)-1);
}

static uint32_t mh_load(uint32_t items, int percent_load_factor)
{
    return (items * percent_load_factor) / 100;
}


static int mh_needs_resize(MHashTable *ht)
{
    int needed = 0;
    if (ht->used >= ht->used_max) {
        needed = 1;
    }
    return needed;
}


static int mh_resize_if_needed(MHashTable *ht)
{
    int err = MHASH_OK;
    if (!ht) {
        err = MHASH_ERROR_BAD_ARGUMENTS;
    } else if (mh_needs_resize(ht)) {
        err = mhash_table_resize(ht, ht->power + 1);
    }
    return err;
}

static int mh_switch_tables(MHashTable *ht)
{
    return 0;
}



static int mh_are_keys_equal(const uint8_t *key_data1, size_t key_size1,
                             const uint8_t *key_data2, size_t key_size2)
{
    return ((key_size1 == key_size2)
         && (memcmp(key_data1, key_data2, key_size1) == 0));
}


static int mh_find_item_in_bucket(CX_VArray *bucket,
                                  const uint8_t *key_data,
                                  size_t key_size,
                                  MHashTableItem **item,
                                  size_t *idx)
{
    int err = MHASH_OK;
    if (!bucket || !key_data || !key_size) {
        err = MHASH_ERROR_BAD_ARGUMENTS;
    } else {
        size_t j = 0, num = CX_varray_length(bucket);
        for (j = 0; j < num; j++) {
            MHashTableItem *cur = CX_varray_get_ref(bucket, j);
            if (cur) {
                if (mh_are_keys_equal(key_data, key_size,
                                      cur->key_data, cur->key_size)) {
                    MH_SET_PTR(item, cur);
                    MH_SET_PTR(idx, j);
                    break;
                }
            } else {
                err = MHASH_ERROR_INTERNAL;
                break; /* something horrible happend */
            }
        }
    }
    return err;
}


static uint32_t mh_hash(MHashTable *ht,
                        const uint8_t *key_data, size_t key_size)
{
    uint32_t mask = mh_mask(ht->power);
    return (hashlib_murmur2(key_data, key_size, MH_SEED) & mask);
}

int mh_add_to_bucket(MHashTable *ht, CX_VArray *bucket,
                     const uint8_t *key_data, size_t key_size, void *data,
                     MHashTableKeyCollisionHandler on_collision)
{
    int err = MHASH_ERROR_GENERIC;
    MHashTableItem new_item = { key_data, key_size, data };
    MHashTableItem *item = NULL; 

    err = mh_find_item_in_bucket(bucket, key_data, key_size, &item, NULL);
    if (!item) {
        err = CX_varray_append(bucket, &new_item);
        if (!err && CX_varray_length(bucket) == 1) {
            ht->used++;
            mh_resize_if_needed(ht);
        }
        ht->count++;
    } else {
        err = on_collision(bucket, item, &new_item);
    }
    mh_migrate_table_step(ht);
    return err;
}

static int mh_migrate_bucket(CX_VArray *old_bucket, MHashTable *new_ht)
{
    CX_VArray *new_bucket = NULL;
    int moved = 0;
    size_t j = 0, num = CX_varray_length(old_bucket);
    for (j = 0; j < num; j++) {
        int err = 0;
        MHashTableItem *cur = CX_varray_get_ref(old_bucket, j);
        if (!cur) {
            continue;
        }
        
        if (!new_bucket) {
            uint32_t idx = mh_hash(new_ht, cur->key_data, cur->key_size);
            new_bucket = mh_get_bucket(new_ht, idx);
        }

        err = mh_add_to_bucket(new_ht, new_bucket,
                               cur->key_data, cur->key_size, cur->data,
                               mh_on_collision_error);
        if (!err) {
            moved++;
        }
    }
    return moved;
}

static int mh_migration_step_done(MHashTable *ht, uint32_t idx)
{
    int done = 0;
    if (idx - ht->moved_items > ht->resize_items_amount) {
        done = 1;
    }
    return done;
}

/* TODO: avoid rehashing */
static int mh_migrate_table_step(MHashTable *ht)
{
    int moved = 0;
    if (ht && ht->shadow) {
        uint32_t j = 0, size = mh_size(ht->power);
        for (j = ht->moved_items;
             j < size && !mh_migration_step_done(ht, j);
             j++) {
            if (ht->buckets[j]) {
                int num = mh_migrate_bucket(ht->buckets[j], ht->shadow);
                ht->buckets[j] = NULL;
                moved += num;
            }
        }
        ht->moved_items += moved;
    }
    return moved;
}



static CX_VArray *mh_get_bucket(MHashTable *ht, uint32_t idx)
{
    CX_VArray *bucket = ht->buckets[idx];

    if (!bucket) {
        bucket = CX_varray_new(MH_DEFAULT_BUCKET_SIZE,
                               sizeof(MHashTableItem));
        if (bucket) {
            ht->buckets[idx] = bucket;
        }
    }

    return bucket;
}


int mh_add(MHashTable *ht,
           const uint8_t *key_data, size_t key_size, void *data,
           MHashTableKeyCollisionHandler on_collision)
{
    uint32_t idx = mh_hash(ht, key_data, key_size);
    CX_VArray *bucket = mh_get_bucket(ht, idx);
    return mh_add_to_bucket(ht, bucket,
                            key_data, key_size, data,
                            on_collision);
}


static int mh_on_collision_error(CX_VArray *bucket,
                                 MHashTableItem *current_item,
                                 MHashTableItem *new_item)
{
    return MHASH_ERROR_KEY_DUPLICATED;
}

static int mh_on_collision_replace(CX_VArray *bucket,
                                   MHashTableItem *current_item,
                                   MHashTableItem *new_item)
{
    void *data = current_item->data;
    current_item->data = new_item->data;
    new_item->data = data;
    return MHASH_OK;
}


static CX_VArray *mh_find_bucket(MHashTable *ht,
                                 const uint8_t *key_data, size_t key_size)
{                                 
    uint32_t idx = mh_hash(ht, key_data, key_size);
    CX_VArray *bucket = ht->buckets[idx];
    return bucket;
}

static CX_VArray *mh_lookup_bucket(MHashTable *ht,
                                   const uint8_t *key_data, size_t key_size)
{
    CX_VArray *bucket = NULL;
    if (ht->shadow) {
        bucket = mh_find_bucket(ht->shadow, key_data, key_size);
    }
    if (!bucket) {
        bucket = mh_find_bucket(ht, key_data, key_size);
    }
    return bucket;
}



MHashTable *mhash_table_new(int initial_power,
                            int percent_load_factor,
                            int resize_items_amount)
{
    MHashTable *ht = NULL;
    MH_CLAMP(initial_power,
             MHASH_MINIMUM_POWER,
             MHASH_MAXIMUM_POWER);
    MH_CLAMP(percent_load_factor, 1, 99);
    /* resize_items_amount is unbounded, < 0 meaning "all" */

    ht = calloc(1, sizeof(struct mhashtable_));
    if (ht) {
        uint32_t size = mh_size(initial_power);
        MH_CLAMP(resize_items_amount, 1, size/2);
        ht->buckets = calloc(size, sizeof(CX_VArray*));
        if (ht->buckets) {
            ht->power = initial_power;
            ht->percent_load_factor = percent_load_factor;
            ht->resize_items_amount = resize_items_amount;
            ht->count = 0;
            ht->used = 0;
            ht->used_max = mh_load(size, percent_load_factor);
            ht->shadow = NULL;
        } else {
            free(ht);
            ht = NULL;
        }
    }
    return ht;
}

int mhash_table_del(MHashTable *ht)
{
    int err = -1;
    if (ht) {
        if (ht->shadow) {
            mhash_table_del(ht->shadow);
        }
        if (ht->buckets) {
            uint32_t j = 0, size = mh_size(ht->power);
            for (j = 0; !err && j < size; j++) {
                CX_VArray *bucket = ht->buckets[j];
                CX_varray_del(bucket);
            }
            free(ht->buckets);
            ht->buckets = NULL;
        }
        free (ht);
        ht = NULL;
        err = 0;
    }
    return err;
}

int mhash_table_resize(MHashTable *ht, int new_power)
{
    int err = MHASH_OK;
    if (!ht || new_power > ht->power) {
        err = MHASH_ERROR_BAD_ARGUMENTS;
    } else if (ht->shadow) {
        err = MHASH_ERROR_RESIZE_IN_PROGRESS;
    } else {
        MH_CLAMP(new_power, ht->power, MHASH_MAXIMUM_POWER);
        ht->shadow = mhash_table_new(new_power,
                                     ht->percent_load_factor,
                                     ht->resize_items_amount);
        if (!ht->shadow) {
            err = MHASH_ERROR_NO_MEMORY;
        } else {
            ht->moved_items = 0;
            err = mh_migrate_table_step(ht);
        }
    }
    return err;
}


uint32_t mhash_table_get_power(MHashTable *ht)
{
    uint32_t power = 0;
    if (ht) {
        if (ht->shadow) {
            power = ht->shadow->power;
        } else {
            power = ht->power;
        }
    }
    return power;
}

uint32_t mhash_table_get_size(MHashTable *ht)
{
    uint32_t size = 0;
    if (ht) {
        if (ht->shadow) {
            size = mh_size(ht->shadow->power);
        } else {
            size = mh_size(ht->power);
        }
    }
    return size;
}

uint32_t mhash_table_get_used(MHashTable *ht)
{
    uint32_t used = 0;
    if (ht) {
        if (ht->shadow) {
            used = ht->shadow->used;
        } else {
            used = ht->used;
        }
    }
    return used;
}

uint32_t mhash_table_get_count(MHashTable *ht)
{
    uint32_t count = 0;
    if (ht) {
        if (ht->shadow) {
            count = ht->shadow->count;
        } else {
            count = ht->count;
        }
    }
    return count;
}


int mhash_table_add(MHashTable *ht,
                    const uint8_t *key_data, size_t key_size, void *data)
{
    int err = MHASH_ERROR_GENERIC;
    if (!ht || !key_data || key_size <= 0 || !data) {
        err = MHASH_ERROR_BAD_ARGUMENTS;
    } else {
        err = mh_add(ht, key_data, key_size, data,
                     mh_on_collision_error);
    }
    return err;
}

int mhash_table_insert(MHashTable *ht,
                       const uint8_t *key_data, size_t key_size,
                       void *data, void **old_data)
{
    int err = MHASH_ERROR_GENERIC;
    if (!ht || !key_data || key_size <= 0 || !data) {
        err = MHASH_ERROR_BAD_ARGUMENTS;
    } else {
        err = mh_add(ht, key_data, key_size, data,
                     mh_on_collision_replace);
    }
    return err;
}


int mhash_table_remove(MHashTable *ht,
                       const uint8_t *key_data, size_t key_size, void **data)
{
    int err = MHASH_ERROR_GENERIC;
    if (!ht || !key_data || key_size <= 0) {
        err = MHASH_ERROR_BAD_ARGUMENTS;
    } else {
        size_t item_idx = 0;
        CX_VArray *bucket = mh_lookup_bucket(ht, key_data, key_size);
        if (bucket) {
            MHashTableItem *item = NULL;
            err = mh_find_item_in_bucket(bucket, key_data, key_size,
                                         &item, &item_idx);
            if (!err) {
                MH_SET_PTR(data, item->data);
                err = CX_varray_remove(bucket, item_idx);
                if (!err && CX_varray_length(bucket) == 0) {
                    ht->used--;
                }
                ht->count--;
            }
        }
        mh_migrate_table_step(ht);
    }
    return err;
}

int mhash_table_find(MHashTable *ht,
                     const uint8_t *key_data, size_t key_size, void **data)
{
    int err = MHASH_OK;
    if (!ht || !key_data || !key_size) {
        err = MHASH_ERROR_BAD_ARGUMENTS;
    } else {
        MHashTableItem *item = NULL;
        CX_VArray *bucket = mh_lookup_bucket(ht, key_data, key_size);
        if (!bucket) {
            err = MHASH_ERROR_INTERNAL;
        } else {
            err = mh_find_item_in_bucket(bucket, key_data, key_size, &item, NULL);

            if (!item) {
                err = MHASH_ERROR_KEY_NOT_FOUND;
            } else {
                MH_SET_PTR(data, item->data);
            }
        }
        mh_migrate_table_step(ht);
    }
    return err;
}



/* TODO

typedef struct mhashtableiterator_ MHashTableIterator;

MHashTableIterator *mhash_table_new_iterator(MHashTable *ht);
int mhash_table_del_iterator(MHashTableIterator *it);

int mhash_table_iterator_done(MHashTableIterator *it);

int mhash_table_iterator_next(MHashTableIterator *it);

int mhash_table_iterator_get_data(MHashTableIterator *it, void **data);
int mhash_table_iterator_get_key(MHashTableIterator *it,
                                 const uint8_t **key_data, size_t *key_size);

*/


static int mh_visit_bucket(CX_VArray *bucket,
                           MHashTableVisitor visitor, void *userdata)
{
    int err = 0;
    size_t k = 0, num = CX_varray_length(bucket);
    for (k = 0; !err && k < num; k++) {
        MHashTableItem *cur = CX_varray_get_ref(bucket, k);
        if (cur) {
            err = visitor(userdata,
                          cur->key_data, cur->key_size,
                          cur->data);
        }
        /* XXX: else? */
    }
    return err;
}

/* TODO: ACID-like visit */
static int mh_visit(MHashTable *ht, MHashTableVisitor visitor, void *userdata)
{
    int err = 0;
    uint32_t j = 0, size = mh_size(ht->power);
    for (j = 0; !err && j < size; j++) {
        CX_VArray *bucket = ht->buckets[j];
        if (bucket) {
            err = mh_visit_bucket(bucket, visitor, userdata);
        }
        /* else we're ok, assuming the bucket wasn't initialized yet */
    }
    return err;
}


int mhash_table_visit(MHashTable *ht, MHashTableVisitor visitor, void *userdata)
{
    int err = MHASH_ERROR_GENERIC;
    if (ht && visitor) {
        err = mh_visit(ht, visitor, userdata);
    }
    return err;
}




/* FIXME: 64-bit counters? */
typedef struct {
    uint32_t collisions_total;
    double collisions_average;

    uint32_t bucket_size_min;
    uint32_t bucket_size_max;

    uint32_t size;
} MHStats;


static int mh_dump_statistics_bucket(MHStats *stats, CX_VArray *bucket)
{
    uint32_t len = CX_varray_length(bucket);
    if (len > stats->bucket_size_max) {
        stats->bucket_size_max = len;
    }
    if (len < stats->bucket_size_min) {
        stats->bucket_size_min = len;
    }
    if (len > 1) {
        stats->collisions_total += (len - 1);
    }
    /* FIXME: recursive mean for collisions_average? */

    return 0;
}


int mhash_table_dump_statistics(MHashTable *ht, FILE *file_out)
{
    int err = 0;
    if (!ht || !file_out) {
        err = -1;
    } else {
        uint32_t j = 0;
        MHStats stats = { 0, 0, 0, 0, 0, };

        stats.bucket_size_min = -1;
        stats.bucket_size_max = 0;
        stats.size = mh_size(ht->power);

        fprintf(file_out, "mhash @%p used=%u size=%u (2^%u): %.3f%%\n",
                ht, ht->used, mh_size(ht->power), ht->power,
                100.0 * ht->used/(double)mh_size(ht->power));
        fflush(file_out);

        for (j = 0; !err && j < stats.size; j++) {
            CX_VArray *bucket = ht->buckets[j];
            if (bucket) {
                err = mh_dump_statistics_bucket(&stats, bucket);
            }
            /* else we're ok, assuming the bucket wasn't initialized yet */
        }
        
        if (!err) {
            /* FIXME temporary (hopefully) */
            stats.collisions_average = stats.collisions_total/(double)stats.size;
            fprintf(file_out, "collisions:\n");
            fprintf(file_out, "  total   = %u\n", stats.collisions_total);
            fprintf(file_out, "  average = %.3f\n", stats.collisions_average);
            fflush(file_out);
        }
    }
    return err;
}


/* vim: set sw=4 ts=4 et */

