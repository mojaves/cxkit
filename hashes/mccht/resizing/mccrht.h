/* 
 * MHashTable - a murmur-based, chanining, composite resizing hash table.
 * (C) 2010-2014 Francesco Romani - fromani at gmail dot com. ZLIB licensed.
 */

/** \file mhash.h
    \brief the MHashTable interface.
*/


#ifndef MHASH_H
#define MHASH_H

#include <stdio.h>

#include <stdint.h>
#include <inttypes.h>


/** \enum Implementation defaults for MHashTable. */
enum {
    MHASH_MINIMUM_POWER               = 5,
    MHASH_MAXIMUM_POWER               = 31,
    MHASH_DEFAULT_LOAD_FACTOR         = 55, /* percent */
    MHASH_DEFAULT_RESIZE_ITEMS_AMOUNT = 1000
};

/** \enum Error codes for the MHashTable functions. */
/** DO NOT rely on their values. ABI isn't guaranteed */
typedef enum {
    MHASH_OK = 0,                   /**< so far, so good.               */
    MHASH_ERROR_GENERIC,            /**< you shouldn't get this.        */
    MHASH_ERROR_NO_MEMORY,          /**< out of memory.                 */
    MHASH_ERROR_INTERNAL,           /**< corrupted MHash data (PANIC!)  */
    MHASH_ERROR_NULL_REFERENCE,     /**< given a bad MHash reference.   */
    MHASH_ERROR_BAD_ARGUMENTS,      /**< given wrong arguments.         */
    MHASH_ERROR_KEY_NOT_FOUND,      /**< requested a missing key.       */
    MHASH_ERROR_KEY_DUPLICATED,     /**< found a duplicated key.        */
    MHASH_ERROR_RESIZE_IN_PROGRESS, /**< hash table resize in progress. */
} MHashError;


typedef struct mhashtable_ MHashTable;

/** 
  \fn Allocates a new MHashTable.
  \param initial_power initial power of the table.
         a given table always has 2**power buckets.
  \param percent_load_factor maximum percentage of the used
         buckets before the resize is triggered.
  \param resize_items_amount amount of elements to move during
         a resize step.
  \return a new MHashTable reference on success, NULL on error
  \see mhash_del
*/
MHashTable *mhash_table_new(int initial_power,
                            int percent_load_factor,
                            int resize_items_amount);

#define mhash_table_new_with_power(PWR) \
	mhash_table_new((PWR), \
			MHASH_DEFAULT_LOAD_FACTOR, \
    			MHASH_DEFAULT_RESIZE_ITEMS_AMOUNT)

/**
  \def Allocates a new MHashTable, using the defaults.
  \param INITIAL_POWER initial power of the table.
  \return a new MHashTable reference on success, NULL on error
  \see mhash_new
*/
#define MHASH_TABLE_NEW(INITIAL_POWER) \
	mhash_table_new((INITIAL_POWER), \
			MHASH_DEFAULT_LOAD_FACTOR, \
			MHASH_DEFAULT_RESIZE_ITEMS_AMOUNT)

/**
  \fn Disposes a MHashTable.
  \param ht a MHashTable reference.
  \return MHASH_OK on success, a MHashError otherwise.
  \see mhash_table_new
*/
int mhash_table_del(MHashTable *ht);

/**
  \fn Explicitely resizes a MHashTable to a greater power.
  \param ht a MHashTable reference.
  \param new_power the new power of the table. Must be greater than
         the previous.
  \return MHASH_OK on success, a MHashError otherwise.
  \see mhash_table_new
*/
int mhash_table_resize(MHashTable *ht, int new_power);

/**
  \fn Explicitely perform a resizing step for a MHashTable.
  \param ht a MHashTable reference.
  \return MHASH_OK on success, a MHashError otherwise.
  \see mhash_table_resize
*/
int mhash_table_force_resize_step(MHashTable *ht);

/**
   \fn Asks an hash table if a resize is in progress.
  \param ht a MHashTable reference.
  \return !0 if the resize is in progress, 0 otherwise
  \see mhash_table_force_resize_step
  \see mhash_table_resize
*/
int mhash_table_is_resizing(MHashTable *ht);

/**
  \fn Gets the current power of a table.
  \param ht a MHashTable reference.
  \return 0 on error, the current power otherwise.
*/
uint32_t mhash_table_get_power(MHashTable *ht);

/**
  \fn Gets the current size of a table.
  \param ht a MHashTable reference.
  \return 0 on error, the current size otherwise.
*/
uint32_t mhash_table_get_size(MHashTable *ht);

/**
  \fn Gets the current amount of items stored in a table.
  \param ht a MHashTable reference.
  \return 0 on error, the current size otherwise.
*/
uint32_t mhash_table_get_used(MHashTable *ht);

/**
  \fn Gets the current amount of used buckets in a table.
  \param ht a MHashTable reference.
  \return 0 on error, the current size otherwise.
*/
uint32_t mhash_table_get_count(MHashTable *ht);


/**
  \fn Adds a new element to the table, aborting on collision.
  This function will fail if the key is already present in the table.
  This function can trigger an implicit table resize.
  NOTICE: this functions DOESN'T COPY the key data NOR the element data.
  The caller MUST TAKE CARE EXPLICITELY of managing the life time of
  both those objects.
  \param ht a MHashTable reference.
  \param key_data constant pointer to the key data for this element.
  \param key_size size in bytes of the key.
  \param data pointer to the data of the element to be added.
  \return MHASH_OK on success, a MHashError otherwise.
  \see mhash_table_resize
*/
int mhash_table_add(MHashTable *ht,
                    const uint8_t *key_data, size_t key_size, void *data);

/**
  \fn Adds a new element to the table, overwriting on collision.
  This function will overwrite the old element if the key is already 
  present in the table.
  This function can trigger an implicit table resize.
  NOTICE: this functions DOESN'T COPY the key data NOR the element data.
  The caller MUST TAKE CARE EXPLICITELY of managing the life time of
  both those objects.
  \param ht a MHashTable reference.
  \param key_data constant pointer to the key data for this element.
  \param key_size size in bytes of the key.
  \param data pointer to the data of the element to be added.
  \param old_data pointer to storage which will held a copy of the
         old element. Can be NULL, in that case the old element is
         silently discarded.
  \return MHASH_OK on success, a MHashError otherwise.
  \see mhash_table_resize
*/
int mhash_table_insert(MHashTable *ht,
                       const uint8_t *key_data, size_t key_size,
                       void *data, void **old_data);

/**
  \fn Removes an element from the table.
  \param ht a MHashTable reference.
  \param key_data constant pointer to the key data for this element.
  \param key_size size in bytes of the key.
  \param data pointer to storage which will held a copy of the
         removed element. Can be NULL, in that case the old element
         is silently discarded.
  \return MHASH_OK on success, a MHashError otherwise.
*/
int mhash_table_remove(MHashTable *ht,
                       const uint8_t *key_data, size_t key_size, void **data);

#define mhash_table_discard(HT, KEY_DATA, KEY_SIZE) \
	mhash_table_remove((HT), (KEY_DATA), (KEY_SIZE), NULL)

/**
  \fn Finds an element into the table.
  \param ht a MHashTable reference.
  \param key_data constant pointer to the key data for this element.
  \param key_size size in bytes of the key.
  \param data pointer to storage which will held a copy of the
         found element. Can be NULL, in that case the found element
         isn't copied at all.
  \return MHASH_OK on success (element found), a MHashError otherwise.
*/
int mhash_table_find(MHashTable *ht,
                     const uint8_t *key_data, size_t key_size, void **data);




typedef struct mhashtableiterator_ MHashTableIterator;

MHashTableIterator *mhash_table_new_iterator(MHashTable *ht);
int mhash_table_del_iterator(MHashTableIterator *it);

int mhash_table_iterator_done(MHashTableIterator *it);

int mhash_table_iterator_next(MHashTableIterator *it);

int mhash_table_iterator_get_data(MHashTableIterator *it, void **data);
int mhash_table_iterator_get_key(MHashTableIterator *it,
                                 const uint8_t **key_data, size_t *key_size);


typedef int (*MHashTableVisitor)(void *userdata,
                                 const uint8_t *key_data, size_t key_size, void *data);

int mhash_table_visit(MHashTable *ht, MHashTableVisitor visitor, void *userdata);


int mhash_table_dump_statistics(MHashTable *ht, FILE *file_out);


#endif // MHASH_H
