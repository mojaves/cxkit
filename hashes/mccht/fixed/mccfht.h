/* 
 * mccfht.h - a murmur-based, chanining, composite, NOT-resizing hash table.
 * (C) 2012-2014 Francesco Romani - fromani at gmail dot com. ZLIB licensed.
 */

/** \file mccfht.h
    \brief the CX_mccfht interface.
*/


#ifndef CX_MCCFHT_H
#define CX_MCCFHT_H

#include "hashtablekit.h"

/** 
  \fn Allocates a new CX_HashTable.
  \param power the power of the table.
         a given table always has 2**power buckets.
  \return a new CX_HashTable reference on success, NULL on error
  \see mccfht_del
*/
CX_HashTable *CX_mccfht_new(int power);

/**
  \fn Gets the current power of a table.
  \param ht a CX_HashTable reference.
  \return 0 on error, the current power otherwise.
*/
uint32_t CX_mccfht_get_power(const CX_HashTable *ht);

/**
  \fn Gets the current size of a table.
  \param ht a CX_HashTable reference.
  \return 0 on error, the current size otherwise.
*/
uint32_t CX_mccfht_get_size(const CX_HashTable *ht);

/**
  \fn Gets the current amount of items stored in a table.
  \param ht a CX_HashTable reference.
  \return 0 on error, the current size otherwise.
*/
uint32_t CX_mccfht_get_used(const CX_HashTable *ht);

/**
  \fn Gets the current amount of used buckets in a table.
  \param ht a CX_HashTable reference.
  \return 0 on error, the current size otherwise.
*/
uint32_t CX_mccfht_get_count(const CX_HashTable *ht);


/**
  \fn Adds a new element to the table, overwriting on collision.
  This function will overwrite the old element if the key is already 
  present in the table.
  NOTICE: this functions DOESN'T COPY the key data NOR the element data.
  The caller MUST TAKE CARE EXPLICITELY of managing the life time of
  both those objects.
  \param ht a CX_HashTable reference.
  \param key_data constant pointer to the key data for this element.
  \param key_size size in bytes of the key.
  \param data pointer to the data of the element to be added.
  \param old_data pointer to storage which will held a copy of the
         old element. Can be NULL, in that case the old element is
         silently discarded.
  \return 0 on success, a CX_HTError otherwise.
*/
int CX_mccfht_insert(CX_HashTable *ht,
                     const uint8_t *key_data, size_t key_size,
                     const void *data, const void **old_data);

/**
  \fn Discards an element from the table.
  \param ht a CX_HashTable reference.
  \param key_data constant pointer to the key data for this element.
  \param key_size size in bytes of the key.
  \return 0 on success, a CX_HTError otherwise.
*/
int CX_mccfht_discard(CX_HashTable *ht,
                      const uint8_t *key_data, size_t key_size);

/*************************************************************************/
/* you should'nt call those directly                                     */

int CX_mccfht_del(CX_HashTable *ht_);
int CX_mccfht_add(CX_HashTable *ht_,
                  const uint8_t *key_data, size_t key_size,
                  const void *data);
int CX_mccfht_remove(CX_HashTable *ht_,
                     const uint8_t *key_data, size_t key_size,
                     const void **data);
int CX_mccfht_find(CX_HashTable *ht_,
                   const uint8_t *key_data, size_t key_size,
                   const void **data);

#endif /* CX_MCFFHT_H */

