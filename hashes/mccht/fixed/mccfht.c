/* 
* CX_HashTable - a murmur-based, chanining, composite resizing hash table.
* (C) 2010-2014 Francesco Romani - fromani at gmail dot com. ZLIB licensed.
*/


#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "arraykit.h"
#include "hashlib.h"
#include "mccfht.h"

typedef struct {
    const uint8_t   *key_data;
    size_t          key_size;
    const void      *data;
} MCCFHTItem;


typedef struct MCCFHT_ {
    CX_HashTable    base;
    CX_VArray       **buckets;

    int             power;

    uint32_t        count;
    uint32_t        used;
} MCCFHT;

enum {
    MH_DEFAULT_BUCKET_SIZE = 8,
    MH_SEED                = 0x00000023, /* FIXME */
    MH_SIGNATURE           = 0x77676770  /* MCCF */
};

typedef int (*HTKeyCollisionHandler)(CX_VArray *bucket,
                                     MCCFHTItem *current_item,
                                     MCCFHTItem *new_item);


/* internal prototypes */
static uint32_t mh_size(uint32_t power);
static uint32_t mh_mask(uint32_t power);
static int mh_are_keys_equal(const uint8_t *key_data1, size_t key_size1,
                             const uint8_t *key_data2, size_t key_size2);
static int mh_find_item_in_bucket(CX_VArray *bucket,
                                  const uint8_t *key_data,
                                  size_t key_size,
                                  MCCFHTItem **item,
                                  size_t *idx);
static uint32_t mh_hash(MCCFHT *ht,
                        const uint8_t *key_data, size_t key_size);
static int mh_add_to_bucket(MCCFHT *ht, CX_VArray *bucket,
                            const uint8_t *key_data, size_t key_size,
                            const void *data,
                            HTKeyCollisionHandler on_collision);
static CX_VArray *mh_get_bucket(MCCFHT *ht, uint32_t idx);
static int mh_add(MCCFHT *ht,
                  const uint8_t *key_data, size_t key_size,
                  const void *data,
                  HTKeyCollisionHandler on_collision);
static int mh_on_collision_error(CX_VArray *bucket,
                                 MCCFHTItem *current_item,
                                 MCCFHTItem *new_item);
static int mh_on_collision_replace(CX_VArray *bucket,
                                   MCCFHTItem *current_item,
                                   MCCFHTItem *new_item);



#define MH_CLAMP(X, LL, UL) do { \
    if ((X) < (LL)) { \
        (X) = (LL); \
    } \
    if ((X) > (UL)) { \
        (X) = (UL); \
    } \
} while (0)

#define MH_SET_PTR(PTR, VAL) do { \
    if ((PTR)) { \
        *(PTR) = (VAL); \
    } \
} while (0)

static uint32_t mh_size(uint32_t power)
{
    return (1 << power);
}

static uint32_t mh_mask(uint32_t power)
{
    return (mh_size(power)-1);
}

static int mh_are_keys_equal(const uint8_t *key_data1, size_t key_size1,
                             const uint8_t *key_data2, size_t key_size2)
{
    return ((key_size1 == key_size2)
         && (memcmp(key_data1, key_data2, key_size1) == 0));
}


static int mh_find_item_in_bucket(CX_VArray *bucket,
                                  const uint8_t *key_data,
                                  size_t key_size,
                                  MCCFHTItem **item,
                                  size_t *idx)
{
    int err = 0;
    if (!bucket || !key_data || !key_size) {
        err = CX_HT_ERROR_BAD_ARGUMENTS;
    } else {
        size_t j = 0, num = CX_varray_length(bucket);
        for (j = 0; j < num; j++) {
            MCCFHTItem *cur = CX_varray_get_ref(bucket, j);
            if (cur) {
                if (mh_are_keys_equal(key_data, key_size,
                                      cur->key_data, cur->key_size)) {
                    MH_SET_PTR(item, cur);
                    MH_SET_PTR(idx, j);
                    break;
                }
            } else {
                err = CX_HT_ERROR_INTERNAL;
                break; /* something horrible happend */
            }
        }
    }
    return err;
}

static uint32_t mh_hash(MCCFHT *ht,
                        const uint8_t *key_data, size_t key_size)
{
    uint32_t mask = mh_mask(ht->power);
    uint32_t hval = ht->base.hash(key_data, key_size, ht->base.seed);
    return (hval & mask);
}

static int mh_add_to_bucket(MCCFHT *ht, CX_VArray *bucket,
                            const uint8_t *key_data, size_t key_size,
                            const void *data,
                            HTKeyCollisionHandler on_collision)
{
    int err = CX_HT_ERROR_GENERIC;
    MCCFHTItem new_item = { key_data, key_size, data };
    MCCFHTItem *item = NULL; 

    err = mh_find_item_in_bucket(bucket, key_data, key_size, &item, NULL);
    if (!item) {
        err = CX_varray_append(bucket, &new_item);
        if (!err && CX_varray_length(bucket) == 1) {
            ht->used++;
        }
        ht->count++;
    } else {
        err = on_collision(bucket, item, &new_item);
    }
    return err;
}

static CX_VArray *mh_get_bucket(MCCFHT *ht, uint32_t idx)
{
    CX_VArray *bucket = ht->buckets[idx];

    if (!bucket) {
        bucket = CX_varray_new(MH_DEFAULT_BUCKET_SIZE,
                               sizeof(MCCFHTItem));
        if (bucket) {
            ht->buckets[idx] = bucket;
        }
    }

    return bucket;
}


static int mh_add(MCCFHT *ht,
                  const uint8_t *key_data, size_t key_size,
                  const void *data,
                  HTKeyCollisionHandler on_collision)
{
    uint32_t idx = mh_hash(ht, key_data, key_size);
    CX_VArray *bucket = mh_get_bucket(ht, idx);
    return mh_add_to_bucket(ht, bucket,
                            key_data, key_size, data,
                            on_collision);
}


static int mh_on_collision_error(CX_VArray *bucket,
                                 MCCFHTItem *current_item,
                                 MCCFHTItem *new_item)
{
    return CX_HT_ERROR_KEY_DUPLICATED;
}

static int mh_on_collision_replace(CX_VArray *bucket,
                                   MCCFHTItem *current_item,
                                   MCCFHTItem *new_item)
{
    const void *data = current_item->data;
    current_item->data = new_item->data;
    new_item->data = data;
    return 0;
}

#define IS_GOOD_SIGN(HT) \
    ((HT) && (HT)->base.signature == MH_SIGNATURE)

#define RETURN_IF_BAD_SIGN(HT) do { \
    if (!(IS_GOOD_SIGN(HT))) { \
        return CX_HT_ERROR_BAD_SIGNATURE; \
    } \
} while (0)

int CX_mccfht_del(CX_HashTable *ht_)
{
    MCCFHT *ht = (MCCFHT *)ht_;
    RETURN_IF_BAD_SIGN(ht);
    if (ht->buckets) {
        uint32_t j = 0, size = mh_size(ht->power);
        for (j = 0; j < size; j++) {
            CX_VArray *bucket = ht->buckets[j];
            CX_varray_del(bucket);
        }
        free(ht->buckets);
        ht->buckets = NULL;
    }
    free (ht);
    ht = NULL;
    return 0;
}

uint32_t CX_mccfht_get_power(const CX_HashTable *ht_)
{
    const MCCFHT *ht = (MCCFHT *)ht_;
    uint32_t power = 0;
    if (IS_GOOD_SIGN(ht)) {
        power = ht->power;
    }
    return power;
}

uint32_t CX_mccfht_get_size(const CX_HashTable *ht_)
{
    const MCCFHT *ht = (MCCFHT *)ht_;
    uint32_t size = 0;
    if (IS_GOOD_SIGN(ht)) {
        size = mh_size(ht->power);
    }
    return size;
}

uint32_t CX_mccfht_get_used(const CX_HashTable *ht_)
{
    const MCCFHT *ht = (MCCFHT *)ht_;
    uint32_t used = 0;
    if (IS_GOOD_SIGN(ht)) {
        used = ht->used;
    }
    return used;
}

uint32_t CX_mccfht_get_count(const CX_HashTable *ht_)
{
    const MCCFHT *ht = (MCCFHT *)ht_;
    uint32_t count = 0;
    if (IS_GOOD_SIGN(ht)) {
        count = ht->count;
    }
    return count;
}

int CX_mccfht_add(CX_HashTable *ht_,
                  const uint8_t *key_data, size_t key_size,
                  const void *data)
{
    int err = CX_HT_ERROR_GENERIC;
    MCCFHT *ht = (MCCFHT *)ht_;
    RETURN_IF_BAD_SIGN(ht);
    if (!key_data || key_size <= 0 || !data) {
        err = CX_HT_ERROR_BAD_ARGUMENTS;
    } else {
        err = mh_add(ht, key_data, key_size, data,
                     mh_on_collision_error);
    }
    return err;
}

int CX_mccfht_insert(CX_HashTable *ht_,
                     const uint8_t *key_data, size_t key_size,
                     const void *data, const void **old_data)
{
    int err = CX_HT_ERROR_GENERIC;
    MCCFHT *ht = (MCCFHT *)ht_;
    RETURN_IF_BAD_SIGN(ht);
    if (!key_data || key_size <= 0 || !data) {
        err = CX_HT_ERROR_BAD_ARGUMENTS;
    } else {
        err = mh_add(ht, key_data, key_size, data,
                     mh_on_collision_replace);
    }
    return err;
}

int CX_mccfht_remove(CX_HashTable *ht_,
                     const uint8_t *key_data, size_t key_size,
                     const void **data)
{
    int err = CX_HT_ERROR_GENERIC;
    MCCFHT *ht = (MCCFHT *)ht_;
    RETURN_IF_BAD_SIGN(ht);
    if (!key_data || key_size <= 0) {
        err = CX_HT_ERROR_BAD_ARGUMENTS;
    } else {
        size_t item_idx = 0;
        uint32_t idx = mh_hash(ht, key_data, key_size);
        CX_VArray *bucket = ht->buckets[idx];
        if (bucket) {
            MCCFHTItem *item = NULL;
            err = mh_find_item_in_bucket(bucket, key_data, key_size,
                                         &item, &item_idx);
            if (!err) {
                MH_SET_PTR(data, item->data);
                err = CX_varray_remove(bucket, item_idx);
                if (!err && CX_varray_length(bucket) == 0) {
                    ht->used--;
                }
                ht->count--;
            }
        }
    }
    return err;
}

int CX_mccfht_discard(CX_HashTable *ht,
                      const uint8_t *key_data, size_t key_size)
{
    return CX_mccfht_remove(ht, key_data, key_size, NULL);
}

int CX_mccfht_find(CX_HashTable *ht_,
                   const uint8_t *key_data, size_t key_size,
                   const void **data)
{
    int err = CX_HT_ERROR_GENERIC;
    MCCFHT *ht = (MCCFHT *)ht_;
    RETURN_IF_BAD_SIGN(ht);
    if (!key_data || !key_size) {
        err = CX_HT_ERROR_BAD_ARGUMENTS;
    } else {
        MCCFHTItem *item = NULL;
        uint32_t idx = mh_hash(ht, key_data, key_size);
        CX_VArray *bucket = ht->buckets[idx];
        if (!bucket) {
            err = CX_HT_ERROR_INTERNAL;
        } else {
            err = mh_find_item_in_bucket(bucket, key_data, key_size, &item, NULL);

            if (!item) {
                err = CX_HT_ERROR_KEY_NOT_FOUND;
            } else {
                MH_SET_PTR(data, item->data);
            }
        }
    }
    return err;
}

/*************************************************************************/

CX_HashTable *CX_mccfht_new(int power)
{
    MCCFHT *ht = NULL;
    MH_CLAMP(power,
             CX_HT_MINIMUM_POWER,
             CX_HT_MAXIMUM_POWER);

    ht = calloc(1, sizeof(MCCFHT));
    if (ht) {
        uint32_t size = mh_size(power);
        ht->buckets = calloc(size, sizeof(CX_VArray*));
        if (ht->buckets) {
            ht->power = power;
            ht->count = 0;
            ht->used = 0;
            ht->base.signature = MH_SIGNATURE;
            ht->base.implementation = ht;
            ht->base.del = CX_mccfht_del;
            ht->base.add = CX_mccfht_add;
            ht->base.remove = CX_mccfht_remove;
            ht->base.find = CX_mccfht_find;
            /* ... */
            ht->base.hash = hashlib_murmur2;
            ht->base.seed = MH_SEED;
        } else {
            free(ht);
            ht = NULL;
        }
    }
    return (CX_HashTable *)ht;
}


/* vim: set sw=4 ts=4 et */

