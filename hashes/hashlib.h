/* 
 * hashlib - hash function library.
 *
 * All code packed here is placed in the public domain.
 *
 * Reimplementation made by me are explictely marked as such,
 * and hereby put in the public domain as well.
 * Francesco Romani { fromani - gmail - com }
 */

#ifndef HASHLIB_H
#define HASHLIB_H

#include <stdint.h>
#include <inttypes.h>

/* TODO: xor_fold */

uint32_t hashlib_murmur2(const void *key, int len, uint32_t seed);

enum {
    /* AKA `Offset basis` in the FNV docs.
       Renamed `seed` for consistency. */
    HASHLIB_FNV1_SEED32 = 2166136261 /* don't use at your own risk */
};

uint32_t hashlib_FNV1a(const void *key, int len, uint32_t seed);


#endif /* HASHLIB_H */

