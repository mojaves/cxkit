
Hashes - experiments with hash table implementations.
======================================================

(c) 2010-2014 - Francesco Romani <fromani . gmail + com>


Overview
--------

This is a collection of experiments with hash table implementations
and hashing in general. There is not a formal stucture, this is more
a code dump. Every subproject is organized in a subdirectory
with its own README, documentation (YMMV) and stuff.

Implementation language will be mostly C and C++.

If not specified otherwise, all code here is licenced under the ZLIB
licence:


	Copyright (c) 2010-2014 - Francesco Romani <fromani . gmail + com>
	   
	This software is provided 'as-is', without any express or implied
	warranty. In no event will the authors be held liable for any damages
	arising from the use of this software.
	
	Permission is granted to anyone to use this software for any purpose,
	including commercial applications, and to alter it and redistribute it
	freely, subject to the following restrictions:
	    
	1. The origin of this software must not be misrepresented; you must not
	claim that you wrote the original software. If you use this software
	in a product, an acknowledgment in the product documentation would be
	appreciated but is not required.
	
	2. Altered source versions must be plainly marked as such, and must not be
	misrepresented as being the original software.
	
	3. This notice may not be removed or altered from any source
	distribution.
	

