/* 
 * Skip List Kit - a simple generic skip list.
 * Skip Lists are described in detail here:
 * ftp://ftp.cs.umd.edu/pub/skipLists/skiplists.pdf
 * (C) 2012-2014 Francesco Romani - fromani at gmail dot com. ZLIB licensed.
 */

/** \file skiplistkit.h
    \brief the Skip List kit interface.
*/

#ifndef CX_SKIPLISTKIT_H
#define CX_SKIPLISTKIT_H

#include "CX_config.h"

/** TODO: allocation hooks */

/** \def opaque for the client */
typedef struct CX_SkipList_ CX_SkipList;

typedef int (*CX_SkipListCmp)(void *ctx,
                              const void *keya, size_t lena,
                              const void *keyb, size_t lenb);

typedef double (*CX_Random)(void *ctx);

/** all the key must have the same length.*/
CX_SkipList* CX_skiplist_new(int maxlevel,
                             CX_SkipListCmp cmp, void *cmpctx,
                             CX_Random randgen, void *randctx);

CX_SkipList *CX_skiplist_clone(const CX_SkipList *sl, int clonedata);

void CX_skiplist_del(CX_SkipList *sl);

int CX_skiplist_insert(CX_SkipList *sl,
                       const void *key, size_t key_len, const void *data);
int CX_skiplist_delete(CX_SkipList *sl,
                       const void *key, size_t key_len, const void **data);

int CX_skiplist_lookup(CX_SkipList *sl,
                       const void *key, size_t key_len, const void **data);

int CX_skiplist_find_next(CX_SkipList *sl,
                          const void *key, size_t key_len, const void **data);
int CX_skiplist_find_prev(CX_SkipList *sl,
                          const void *key, size_t key_len, const void **data);

int CX_skiplist_length(CX_SkipList *sl);

typedef struct CX_SkipListIter_ CX_SkipListIter;

CX_SkipListIter *CX_skiplist_iter_new_from_head(CX_SkipList *sl);
CX_SkipListIter *CX_skiplist_iter_new_from_key(CX_SkipList *sl,
                                               const void *key, size_t key_len);
CX_SkipListIter *CX_skiplist_iter_new_from_key_rev(CX_SkipList *sl,
                                                   const void *key, size_t key_len);
void CX_skiplist_iter_del(CX_SkipListIter *it);
int CX_skiplist_iter_get(CX_SkipListIter *it, const void **data);
int CX_skiplist_iter_next(CX_SkipListIter *it);
int CX_skiplist_iter_has_next(CX_SkipListIter *it);


#endif  /* CX_SKIPLISTKIT_H */

