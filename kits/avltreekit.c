/* 
 * AVL Tree Kit - a simple generic AVL Tree.
 * (C) 2012-2014 Francesco Romani - fromani at gmail dot com. ZLIB licensed.
 */

/** \file avltreekit.c
    \brief the AVL tree kit implementation.
*/


#include "avltreekit.h"

typedef struct CX_AVLNode_ CX_AVLNode;
struct CX_AVLNode_ {
    int         balance;
    const void *key;
    const void *data;
    CX_AVLNode *parent;
    CX_AVLNode *left;
    CX_AVLNode *right;
};

//typedef int (*CX_AVLTreeCmp)(const void *a, const void *b);
struct CX_AVLTree_ {
    AVLNode       *root;
    CX_AVLTreeCmp  compare;
};


static CX_AVLNode *new_node(const void *key, const void *data)
{
    return NULL;
}

static void del_node(CX_AVLNode *node)
{
    return;
}


CX_AVLTree* CX_avltree_new(CX_AVLTreeCmp cmp)
{
    CX_AVLTree *avl = calloc(1, sizeof(struct CX_AVLTree));
    if (avl) {
        avl->root    = NULL;
        avl->compare = cmp;
    }
    return avl;
}

void CX_avltree_del(CX_AVLTree *avl)
{
    free(avl);
    return;
}

static CX_AVLNode *get_child(CX_AVLNode *node, int cmp)
{
    CX_AVLNode *node = NULL;
    if (cmp < 0) {
        node = node->left;
    } else {
        node = node->right;
    }
    return node;
}

static int set_child(CX_AVLNode *node, int cmp, CX_AVLNode *child)
{
    if (cmp < 0) {
        node->left = child;
    } else {
        node->right = child;
    }
    return child_balance(node);
}


typedef struct AVLLookup AVLLookup;
struct AVLLookup {
    CX_AVLNode *parent;
    const void *data;
    int         cmp;
};

static int lookup_node(CX_AVLTree *avl, const void *key, AVLLookup *res)
{
    int got = 0;
    CX_AVLNode *node = avl->root;
    res->parent = NULL;
    while (node && !got) {
        res->cmp = avl->compare(node->key, key);
        if (res->cmp == 0) {
            res->data = node->data;
            got = 1;
        }
        res->parent = node;
        node = get_child(node, res->cmp);
    }
    return got;
}


static int child_balance(CX_AVLNode *node)
{
    int lw = (node->left)  ?node->left->balance  :0;
    int rw = (node->right) ?node->right->balance :0;
    node->balance = (lw - rw);
    return node->balance;
}

static int rotate_left(CX_AVLTree *avl, CX_AVLNode *node)
{
    return 0;
}

static int rotate_right(CX_AVLTree *avl, CX_AVLNode *node)
{
    return 0;
}

static int avltree_rebalance(CX_AVLTree *avl, const CX_AVLNode *node)
{
    RETURN_ERR_IF_NULL(avl);
    while (node) {
        if (node->balance < -1) {
            CX_AVLNode *right = node->right;
            if (right->balance == -1) {
                ; /* pass */
            }
            if (right->balance == +1) {
                rotate_right(avl, right);
            }
            rotate_left(avl, node);
        }
        if (node->balance > +1) {
            CX_AVLNode *left = node->left;
            if (left->balance == +1) {
                ; /* pass */
            }
            if (left->balance == -1) {
                rotate_left(avl, left);
            }
            rotate_right(avl, node);
        }
        node = node->parent; 
    }
    return 0;
}

int CX_avltree_insert(CX_AVLTree *avl, const void *key, const void *data)
{
    int res = -1;
    int got = 0;
    AVLLookup look;
    RETURN_ERR_IF_NULL(avl);
    RETURN_ERR_IF_NULL(key);

    got = lookup_node(avl, key, &look);
    if (got) {
        res = got;
    } else {
        CX_AVLNode *child = NULL;
        if (!look.parent) {
            look.parent = avl->root;
        }
        child = new_node(key, data);
        if (child) {
            set_child(look.parent, look.cmp, child);
            res = avltree_rebalance(avl, look.parent);
        }
    }
    return res;
}

#define RETURN_NULL_IF_NULL(PTR) do { \
    if ((PTR) == NULL) { \
        return NULL; \
    } \
} while (0)


#define RETURN_ERR_IF_NULL(PTR) do { \
    if ((PTR) == NULL) { \
        return -1; \
    } \
} while (0)


int CX_avltree_delete(CX_AVLTree *avl, const void *key, const void **data)
{
    RETURN_ERR_IF_NULL(avl);
    RETURN_ERR_IF_NULL(key);

    return 0;
}

#define SETPTR(PTR, VAL) do { \
    if ((PTR)) { \
        *(PTR) = (VAL); \
    } \
} while (0)

int CX_avltree_lookup(CX_AVLTree *avl, const void *key, int *found, const void **data)
{
    AVLLookup look;
    RETURN_ERR_IF_NULL(avl);
    RETURN_ERR_IF_NULL(key);

    lookup_node(avl, key, &look);
    SETPTR(data, look.data);
    return 0;
}

static CX_AVLNode *find_next(CX_AVLTree *avl, CX_AVLNode *node, CX_AVLNode *parent)
{
    CX_AVLNode *next = NULL;
    RETURN_NULL_IF_NULL(avl);
    RETURN_NULL_IF_NULL(node);
    RETURN_NULL_IF_NULL(parent);

    if (node == parent->left) {
        next = parent;
    } else /*if (node == parent->right)*/{
        CX_AVLNode *grandparent = NULL;
        do {
            grandparent = parent->parent;
        } while (grandparent && parent == grandparent->right);
        next = grandparent;
    }
    return next;
}


int CX_avltree_find_next(CX_AVLTree *avl, const void *key, const void **nextkey, const void **data)
{
    int got = 0;
    CX_AVLNode *node = avl->root;
    CX_AVLNode *parent = NULL;
    RETURN_ERR_IF_NULL(avl);
    RETURN_ERR_IF_NULL(key);

    while (node && !got) {
        int cmp = avl->compare(node->key, key);
        if (cmp == 0) {
            if (parent) {
                CX_AVLNode *next = find_next(avl, node, parent);
                if (next) {
                    SETPTR(nextkey, next->key);
                    SETPTR(data,    next->data);
                }
            } else { /* only the root, or maybe just a bug. */
                SETPTR(nextkey, NULL);
                SETPTR(data,    NULL);
            }
            got = 1;
        }
        parent = node;
        node = get_child(node, cmp);
    }
    return 0;
}


/* EOF */

