/* 
 * Skip List Kit - a simple generic skip list.
 * Skip Lists are described in detail here:
 * ftp://ftp.cs.umd.edu/pub/skipLists/skiplists.pdf
 * (C) 2012-2014 Francesco Romani - fromani at gmail dot com. ZLIB licensed.
 */

/** \file skiplistkit.c
    \brief the Skip List kit implementation.
*/

#include <stdlib.h>
#include <stdio.h>

#include "skiplistkit.h"


/* FIXME enforce */
#define CX_SKIPLIST_LEVEL_MAX   32
#define CX_SKIPLIST_P           (1.0/4.0)

typedef unsigned char uint8_t;

typedef struct CX_SkipNode_ CX_SkipNode;

typedef struct CX_SkipForward_ {
    CX_SkipNode *forward;
} CX_SkipForward;

struct CX_SkipNode_ {
    const void *key;
    const void *data;
    CX_SkipForward *levels;
    size_t key_len; /* here to avoid holes */
    int level;
    CX_SkipNode *backward;
};

struct CX_SkipList_ {
    void *(*memallocz)(void *ctx, size_t size);
    void (*memfree)(void *ctx, void *ptr);
    void *memctx;

    CX_SkipListCmp cmp;
    void *cmpctx;

    CX_Random randgen;
    void *randctx;

    CX_SkipNode *header;
    unsigned long length;
    int level;
    int maxlevel;
};

static void *sl_memallocz(void *ctx, size_t size)
{
    return calloc(1, size);
}

static void sl_memfree(void *ctx, void *ptr)
{
    return free(ptr);
}


static int sl_level(CX_SkipList *sl)
{
    int level = 1;
    while (sl->randgen(sl->randctx) < CX_SKIPLIST_P
        && level < sl->maxlevel) {
        level += 1;
    }
    return (level < CX_SKIPLIST_LEVEL_MAX ?level :CX_SKIPLIST_LEVEL_MAX);
}

static CX_SkipNode *sl_node_init(CX_SkipNode *sn, uint8_t *ptr, int level,
                                 const void *key, size_t key_len,
                                 const void *data,
                                 CX_SkipNode *backward)
{
    sn = (CX_SkipNode *)ptr;
    sn->levels = (CX_SkipForward *)(ptr + sizeof(*sn));
    sn->level = level;
    sn->data = data;
    sn->key = key;
    sn->key_len = key_len;
    sn->backward = backward;
    return sn;
}

static
CX_SkipNode *sl_node_new(CX_SkipList *sl, int level,
                         const void *key, size_t key_len, const void *data,
                         CX_SkipNode *backward)
{
    CX_SkipNode *sn = NULL;
    if (sl) {
        uint8_t *ptr = sl->memallocz(sl->memctx,
                                     sizeof(*sn) + level * sizeof(CX_SkipForward));
        if (ptr) {
            sn = sl_node_init(sn, ptr, level, key, key_len, data, backward);
        }
    }
    return sn;
}

static
void sl_node_del(CX_SkipList *sl, CX_SkipNode *sn)
{
    if (sl && sn) {
        sl->memfree(sl->memctx, sn);
    }
    return;
}

int CX_skiplist_length(CX_SkipList *sl)
{
    int len = 0;
    if (sl) {
        len = sl->length;
    }
    return len;
}


CX_SkipList* CX_skiplist_new(int maxlevel,
                             CX_SkipListCmp cmp, void *cmpctx,
                             CX_Random randgen, void *randctx)
{
    CX_SkipList *sl = NULL;
    uint8_t *ptr = sl_memallocz(NULL, sizeof(*sl) + sizeof(CX_SkipNode)
                                      + sizeof(CX_SkipForward) * maxlevel);
    if (ptr) {
        sl = (CX_SkipList *)ptr;
        sl->memallocz = sl_memallocz;
        sl->memfree   = sl_memfree;
        sl->memctx    = NULL;
        sl->cmp       = cmp;
        sl->cmpctx    = cmpctx;
        sl->randgen   = randgen;
        sl->randctx   = randctx;
        sl->maxlevel  = maxlevel;
        sl->level     = 1;
        sl->length    = 0;
        sl->header    = sl_node_init(sl->header,
                                     ptr + sizeof(*sl),
                                     maxlevel, NULL, 0, NULL, NULL);
    }
    return sl;
}

CX_SkipList *CX_skiplist_clone(const CX_SkipList *sl, int clonedata)
{
    CX_SkipList *nsl = CX_skiplist_new(sl->maxlevel,
                                       sl->cmp, sl->cmpctx,
                                       sl->randgen, sl->randctx);
    if (nsl && clonedata) {
        int err = 0;
        const CX_SkipNode *x = NULL;
        // FIXME: find a smarter way
        for (x = sl->header; !err && x; x = x->levels[0].forward) {
            err = CX_skiplist_insert(nsl, x->key, x->key_len, x->data);
        }
    }
    return nsl;
}

void CX_skiplist_del(CX_SkipList *sl)
{
    /* TODO */
    return;
}

#define SETPTR(PTR, VAL) do { \
    if ((PTR)) { \
        *(PTR) = (VAL); \
    } \
} while (0)

#define RETURN_IF_NULL(PTR, VAL) do { \
    if ((PTR) == NULL) { \
        return (VAL); \
    } \
} while (0)

static CX_SkipNode *sl_find(CX_SkipList *sl, 
                            CX_SkipNode **update,
                            const void *key, size_t key_len,
                            CX_SkipNode **backward)
{
    CX_SkipNode *found = NULL;
    CX_SkipNode *x = NULL;
    int j = 0;
 
    x = sl->header;
    for (j = sl->level - 1; j >= 0; j--) {
        while (x->levels[j].forward
            && sl->cmp(sl->cmpctx,
                       x->levels[j].forward->key, x->levels[j].forward->key_len,
                       key, key_len) < 0) {
            x = x->levels[j].forward;
        }
        update[j] = x;
    }
    SETPTR(backward, x);
    x = x->levels[0].forward;
    if (x && sl->cmp(sl->cmpctx, x->key, x->key_len, key, key_len) == 0) {
        found = x;
    }
    return found;
}

int CX_skiplist_lookup(CX_SkipList *sl,
                       const void *key, size_t key_len, const void **data)
{
    CX_SkipNode *update[CX_SKIPLIST_LEVEL_MAX] = { NULL };
    CX_SkipNode *x = NULL;
    int err = 0;

    RETURN_IF_NULL(sl,  -1);
    RETURN_IF_NULL(key, -1);

    x = sl_find(sl, update, key, key_len, 0); 
    if (x) {
        SETPTR(data, x->data);
    } else{
        SETPTR(data, NULL);
        err = 1;
    }

    return err;
}

#ifdef SKL_DEBUG
static void dump_update(CX_SkipNode **update, int N)
{
    int j;
    for (j = 0; j < N; j++) {
        fprintf(stderr, "update[%i] = %p\n", j, update[j]);
    }
    return;
}
#endif

static int sl_insert(CX_SkipList *sl,
                     CX_SkipNode **update,
                     const void *key, size_t key_len, const void *data,
                     CX_SkipNode *backward)
{
    CX_SkipNode *x = NULL;
    int j = 0, level = sl_level(sl);
    if (level > sl->level) {
        int j = 0;
        for (j = sl->level; j < level; j++) {
            update[j] = sl->header;
        }
        sl->level = level;
    }
#ifdef SKL_DEBUG
    dump_update(update, sl->level);
#endif
    x = sl_node_new(sl, level, key, key_len, data, backward); // FIXME
    for (j = 0; j < level; j++) {
        x->levels[j].forward = update[j]->levels[j].forward;
        update[j]->levels[j].forward = x;
    }
    sl->length++;
    return 0; /* FIXME */
}

int CX_skiplist_insert(CX_SkipList *sl,
                       const void *key, size_t key_len, const void *data)
{
    CX_SkipNode *update[CX_SKIPLIST_LEVEL_MAX] = { NULL };
    CX_SkipNode *x = NULL;
    CX_SkipNode *y = NULL;
    int err = 0;
 
    RETURN_IF_NULL(sl,  -1);
    RETURN_IF_NULL(key, -1);

    x = sl_find(sl, update, key, key_len, &y);
#ifdef SKL_DEBUG
    dump_update(update, CX_SKIPLIST_LEVEL_MAX);
#endif
    if (x) {
        x->data = data;
        err = 1;
    } else {
        err = sl_insert(sl, update, key, key_len, data, y);
    }
    return err;
}

int CX_skiplist_delete(CX_SkipList *sl,
                       const void *key, size_t key_len, const void **data)
{
    CX_SkipNode *update[CX_SKIPLIST_LEVEL_MAX] = { NULL };
    CX_SkipNode *x = NULL;
    int err = 0;
    int j = 0;
 
    RETURN_IF_NULL(sl,  -1);
    RETURN_IF_NULL(key, -1);

    x = sl_find(sl, update, key, key_len, 0);
    if (x) {
        for (j = 0; j < sl->level; j++) {
            if (update[j]->levels[j].forward != x) {
                break;
            } else {
                update[j]->levels[j].forward = x->levels[j].forward;
            }
        }
        SETPTR(data, x->data);
        sl_node_del(sl, x);
        while (sl->level > 1
            && sl->header->levels[sl->level-1].forward == NULL) {
            sl->level--;
        }
        sl->length--;
        err = 0;
    } else {
        SETPTR(data, NULL);
        err = 1;
    }
    return err;
}

int CX_skiplist_find_next(CX_SkipList *sl,
                          const void *key, size_t key_len, const void **data)
{
    return -1; /* TODO */
}

int CX_skiplist_find_prev(CX_SkipList *sl,
                          const void *key, size_t key_len, const void **data)
{
    return -1; /* TODO */
}

struct CX_SkipListIter_ {
    CX_SkipList *parent;
    CX_SkipNode *node;
    int forward;
};

CX_SkipListIter *CX_skiplist_iter_new_from_head(CX_SkipList *sl)
{
    CX_SkipListIter *it = calloc(1, sizeof(struct CX_SkipListIter_));
    if (it) {
        it->parent = sl;
        it->node = sl->header->levels[0].forward;
        it->forward = 1;
    }
    return it;
}

CX_SkipListIter *CX_skiplist_iter_new_from_key(CX_SkipList *sl,
                                               const void *key, size_t key_len)
{
    CX_SkipListIter *it = CX_skiplist_iter_new_from_head(sl);
    if (it) {
        CX_SkipNode *update[CX_SKIPLIST_LEVEL_MAX] = { NULL };
        it->node = sl_find(sl, update, key, key_len, 0);
    }
    return it;
}

CX_SkipListIter *CX_skiplist_iter_new_from_key_rev(CX_SkipList *sl,
                                                   const void *key, size_t key_len)
{
    CX_SkipListIter *it = CX_skiplist_iter_new_from_key(sl, key, key_len);
    if (it) {
        it->forward = 0;
    }
    return it;
}

void CX_skiplist_iter_del(CX_SkipListIter *it)
{
    free(it);
}

int CX_skiplist_iter_get(CX_SkipListIter *it, const void **data)
{
    int err = -1;
    if (it && it->node) {
        *data = it->node->data;
        err = 0;
    }
    return err;
}

int CX_skiplist_iter_next(CX_SkipListIter *it)
{
    int err = -1;
    if (it && it->node) {
        if (it->forward) {
            it->node = it->node->levels[0].forward;
        } else {
            it->node = it->node->backward;
        }
        err = 0;
    }
    return err;
}

int CX_skiplist_iter_has_next(CX_SkipListIter *it)
{
    int next = 0;
    if (it && it->node) {
        if (it->forward) {
            next = (it->node->levels[0].forward != NULL);
        } else {
            next = (it->node->backward != NULL);
        }
    }
    return next;
}


/* EOF */

