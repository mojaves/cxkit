/* 
 * AVL Tree Kit - a simple generic AVL Tree.
 * (C) 2012-2014 Francesco Romani - fromani at gmail dot com. ZLIB licensed.
 */

/** \file avltreekit.h
    \brief the AVL tree kit interface.
*/

#ifndef CX_AVLTREEKIT_H
#define CX_AVLTREEKIT_H

#include "CX_config.h"

/** \def opaque for the client */
typedef struct CX_AVLTree_ CX_AVLTree;

typedef int (*CX_AVLTreeCmp)(const void *a, const void *b);


CX_AVLTree* CX_avltree_new(CX_AVLTreeCmp cmp)
void CX_avltree_del(CX_AVLTree *avl);

int CX_avltree_insert(CX_AVLTree *avl, const void *key, const void *data);
int CX_avltree_delete(CX_AVLTree *avl, const void *key, const void **data);

int CX_avltree_lookup(CX_AVLTree *avl, const void *key, int *found, const void **data);
int CX_avltree_find_next(CX_AVLTree *avl, const void *key, const void **nextkey, const void **data);

#endif  /* CX_AVLTREEKIT_H */

