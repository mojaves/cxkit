/*
 * check_varray.c - unit tests for varray.
 * (C) 2010-2014 Francesco Romani - fromani at gmail dot com. ZLIB licensed.
 */

#include <strings.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <stdio.h>

#include <check.h>

#include "arraykit.h"



/*************************************************************************/

START_TEST(test_arrayNewZeroSizes)
{
    CX_VArray *array = CX_varray_new(0, 0);
    fail_unless(array == NULL, "created with zero size");
    /* yes, there is a memleak if the test fails*/
}
END_TEST

START_TEST(test_arrayNewZeroItemSize)
{
    CX_VArray *array = CX_varray_new(23, 0);
    fail_unless(array != NULL, "NOT created with zero element size");
    fail_unless(CX_varray_element_size(array) == sizeof(void *),
                "unrecovered zero element size");
    CX_varray_del(array);
    /* yes, there is a memleak if the test fails*/
}
END_TEST

START_TEST(test_arrayEmptyLength)
{
    CX_VArray *array = CX_varray_new(23, 0);
    int len = CX_varray_length(array);
    CX_varray_del(array);

/*    fail_unless(ret == CX_VARRAY_OK, "failed while getting length");*/
    fail_unless(len == 0, "newly created array has non-zero length=%i", len);
}
END_TEST

START_TEST(test_arrayAppendOne)
{
    CX_VArray *array = CX_varray_new(23, sizeof(int));
    int ret = -1;
    int value = 42;

    ret = CX_varray_append(array, &value);
    fail_unless(ret == CX_VARRAY_OK, "array append(%i) failed", value);
    CX_varray_del(array);
}
END_TEST


START_TEST(test_arrayLengthAfterAppendOne)
{
    CX_VArray *array = CX_varray_new(23, sizeof(int));
    int ret = -1;
    int value = 42, len = 0;

    ret = CX_varray_append(array, &value);
    len = CX_varray_length(array);
    fail_unless(len == 1, "array after append has inconsistent length=%i", len);
    CX_varray_del(array);
}
END_TEST

START_TEST(test_arrayGetAfterAppendOne)
{
    CX_VArray *array = CX_varray_new(23, sizeof(int));
    int ret = -1;
    int value = 42;
    int *elem = NULL;

    ret = CX_varray_append(array, &value);
    elem = CX_varray_get_ref(array, 0);
    fail_unless(elem != NULL, "array get(%i) failed", 1);
    fail_unless(*elem == value, "got=%i expected=%i", *elem, value);
    CX_varray_del(array);
}
END_TEST

START_TEST(test_arrayGetPastLength)
{
    CX_VArray *array = CX_varray_new(23, sizeof(int));
    int ret = -1;
    int value = 42;
    int *elem = NULL;

    ret = CX_varray_append(array, &value);
    elem = CX_varray_get_ref(array, 12);
    fail_if(elem == NULL, "array get past length failed");
    CX_varray_del(array);
}
END_TEST

START_TEST(test_arrayGetFromEmpty)
{
    CX_VArray *array = CX_varray_new(23, sizeof(int));
    int *elem = NULL;

    elem = CX_varray_get_ref(array, 0);
    fail_unless(elem == NULL, "array get from empty succeded");
    CX_varray_del(array);
}
END_TEST

START_TEST(test_arrayAppendN)
{
    CX_VArray *array = CX_varray_new(23, sizeof(int));
    int ret = -1;
    int j = 0, len = 0,  LEN = 11;

    for (j = 0; j < LEN; j++) {
        ret = CX_varray_append(array, &j);
        fail_unless(ret == CX_VARRAY_OK, "array append(%i) failed", j);
    }
    len = CX_varray_length(array);
    fail_unless(len == LEN,
                "array length mismatch obtained=%i expected=%i",
                len, LEN);
    CX_varray_del(array);
}
END_TEST

START_TEST(test_arrayAppendNAndGet)
{
    CX_VArray *array = CX_varray_new(23, sizeof(int));
    int ret = -1;
    int j = 0,  LEN = 11;

    for (j = 0; j < LEN; j++) {
        ret = CX_varray_append(array, &j);
    }
    for (j = 0; j < LEN; j++) {
        int *elem = CX_varray_get_ref(array, j);
        fail_unless(elem != NULL, "lost the %i-th element", j);
        fail_unless(*elem == j, "%i-th element mismatch found=%i", j, *elem);
    }
    CX_varray_del(array);
}
END_TEST

START_TEST(test_arrayAppendNWithGrow)
{
    CX_VArray *array = CX_varray_new(1, sizeof(int));
    int ret = -1;
    int j = 0, len = 0,  LEN = 11;

    for (j = 0; j < LEN; j++) {
        ret = CX_varray_append(array, &j);
        fail_unless(ret == CX_VARRAY_OK, "array append(%i) failed", j);
    }
    len = CX_varray_length(array);
    fail_unless(len == LEN,
                "array length mismatch obtained=%i expected=%i",
                len, LEN);
    CX_varray_del(array);
}
END_TEST

START_TEST(test_arrayAppendNWithGrowAndGetEarly)
{
    CX_VArray *array = CX_varray_new(1, sizeof(int));
    int ret = -1;
    int j = 0,  LEN = 11;
    int *elem = NULL;

    for (j = 0; j < LEN; j++) {
        ret = CX_varray_append(array, &j);
        elem = CX_varray_get_ref(array, j);
        fail_unless(elem != NULL, "lost the %i-th element", j);
        fail_unless(*elem == j, "%i-th element mismatch found=%i", j, *elem);
    }
    CX_varray_del(array);
}
END_TEST

START_TEST(test_arrayAppendNWithGrowAndGet)
{
    CX_VArray *array = CX_varray_new(1, sizeof(int));
    int ret = -1;
    int j = 0,  LEN = 11;

    for (j = 0; j < LEN; j++) {
        ret = CX_varray_append(array, &j);
    }
    for (j = 0; j < LEN; j++) {
        int *elem = CX_varray_get_ref(array, j);
        fail_unless(elem != NULL, "lost the %i-th element", j);
        fail_unless(*elem == j, "%i-th element mismatch found=%i", j, *elem);
    }
    CX_varray_del(array);
}
END_TEST


/*************************************************************************/

TCase *makeTestCaseArray(void)
{
    TCase *tcArray = tcase_create("CXKit.VArray");
    tcase_add_test(tcArray, test_arrayNewZeroSizes);
    tcase_add_test(tcArray, test_arrayNewZeroItemSize);
    tcase_add_test(tcArray, test_arrayEmptyLength);
    tcase_add_test(tcArray, test_arrayAppendOne);
    tcase_add_test(tcArray, test_arrayLengthAfterAppendOne);
    tcase_add_test(tcArray, test_arrayGetAfterAppendOne);
    tcase_add_test(tcArray, test_arrayGetPastLength);
    tcase_add_test(tcArray, test_arrayGetFromEmpty);
    tcase_add_test(tcArray, test_arrayAppendN);
    tcase_add_test(tcArray, test_arrayAppendNAndGet);
    tcase_add_test(tcArray, test_arrayAppendNWithGrow);
    tcase_add_test(tcArray, test_arrayAppendNWithGrowAndGetEarly);
    tcase_add_test(tcArray, test_arrayAppendNWithGrowAndGet);
    return tcArray;
}



static Suite *makeSuiteArray(void)
{
    TCase *tcArray = makeTestCaseArray();
    Suite *s = suite_create("CXKit.VArray");
    suite_add_tcase(s, tcArray);
    return s;
}

/*************************************************************************/

int main(int argc, char *argv[])
{
    int number_failed = 0;

    Suite *s = makeSuiteArray();
    SRunner *sr = srunner_create(s);

    srunner_run_all(sr, CK_ENV);
    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}

/*************************************************************************/

/* vim: set ts=4 sw=4 et */
/* EOF */

