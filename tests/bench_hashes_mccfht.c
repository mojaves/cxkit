/*
 * bench_hashes_mhash.c - simplistic benchmark for hashes::mhash /1.
 * (C) 2012-2014 Francesco Romani - fromani at gmail dot com. ZLIB licensed.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>


#include "stringkit.h"
#include "mccht/fixed/mccfht.h"


enum {
    LINE_SIZE = 1024
};


static int dummy_value = 42;



void usage(const char *exe)
{
    fprintf(stderr, "usage: %s hash_power\n", exe);
    fprintf(stderr, "  hash_power = [2,31]\n");
}

static uint8_t *dup_key(const char *key, size_t len)
{
    uint8_t *nkey = NULL;
    if (len) {
        nkey = calloc(len + 1, 1);
        if (nkey) {
            CX_strlcpy((char*)nkey, key, len);
        }
    }
    return nkey;
}

int main(int argc, char *argv[])
{
    int power = 0;
    CX_HashTable *ht = NULL;

    if (argc != 2) {
        usage(argv[0]);
        exit(1);
    } else {
        power = atoi(argv[1]);
    }

    ht = CX_mccfht_new((uint32_t)power); /* ugh */
    if (ht) {
        char line_buffer[LINE_SIZE] = { '\0' };
        int err = 0;

        while (fgets(line_buffer, LINE_SIZE, stdin)) {
            size_t len = 0;
            CX_strstrip(line_buffer);
            len = strlen(line_buffer);
            uint8_t *key = dup_key(line_buffer, len);

            if (key) {
                err = CX_mccfht_add(ht, key, len, &dummy_value);
                if (err) {
                    /*fprintf(stderr,
                              "error=[%i] adding key=[%s]\n",
                              err, key);
                     */
                }
            }
        }

/*        if (!err) {
            err = mhash_table_dump_statistics(ht, stdout);
        }
*/
    }

    return 0;
}

/* vim: set et sw=4 ts=4 */
/* EOF */

