/*
 * check_hashes_mhash.c - unit tests for hashes::mhash.
 * (C) 2012-2014 Francesco Romani - fromani at gmail dot com. ZLIB licensed.
 */

#include <strings.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <stdio.h>

#include <check.h>

#include "mccht/fixed/mccfht.h"


/*************************************************************************/

START_TEST(test_hashes_mhashNew)
{
    int ret;
    CX_HashTable *mh = CX_mccfht_new(5);
    fail_if(mh == NULL, "CX_mccfht_new(5) failed");

    ret = CX_mccfht_del(mh);
    
    fail_if(ret != 0, "CX_mccfht_del() failed");
}
END_TEST

START_TEST(test_hashes_mhashNewNegativePower)
{
    int ret;
    CX_HashTable *mh = CX_mccfht_new(-1);
    fail_if(mh == NULL, "CX_mccfht_new(-1) failed");
    fail_if(CX_mccfht_get_power(mh) < CX_HT_MINIMUM_POWER);

    ret = CX_mccfht_del(mh);
    
    fail_if(ret != 0, "CX_mccfht_del() failed");
}
END_TEST

START_TEST(test_hashes_mhashNewExcessivePower)
{
    int ret;
    CX_HashTable *mh = CX_mccfht_new(99999);
    fail_if(mh == NULL, "CX_mccfht_new(99999) failed");
    fail_if(CX_mccfht_get_power(mh) > CX_HT_MAXIMUM_POWER);

    ret = CX_mccfht_del(mh);
    
    fail_if(ret != 0, "CX_mccfht_del() failed");
}
END_TEST

START_TEST(test_hashes_mhashAddOne)
{
    uint32_t used = 0;
    int value = 42;
    const uint8_t *key = (const uint8_t*)&value;
    void *data = &value;
    int ret;
    CX_HashTable *mh = CX_mccfht_new(6);
    fail_if(mh == NULL, "CX_mccfht_new(6) failed");

    ret = CX_mccfht_add(mh, key, sizeof(value), data);
    fail_if(ret != 0, "CX_mccfht_add(42) failed");

    used = CX_mccfht_get_used(mh);
    fail_if(used != 1, "mhash used %lu instead of %lu", used, 1);

    ret = CX_mccfht_del(mh);
    
    fail_if(ret != 0, "CX_mccfht_del() failed");
}
END_TEST

START_TEST(test_hashes_mhashAddOneGet)
{
    uint32_t used = 0;
    int value = 42;
    const uint8_t *key = (const uint8_t*)&value;
    void *data = &value;
    void *founddata = NULL;
    int ret;
    CX_HashTable *mh = CX_mccfht_new(6);
    ret = CX_mccfht_add(mh, key, sizeof(value), data);

    ret = CX_mccfht_find(mh, key, sizeof(value), &founddata);
    fail_if(ret != 0, "CX_mccfht_find(42) failed");
    fail_if(founddata == NULL, "CX_mccfht_find(42) not found!");
    fail_if(*((int *)founddata) != value,
            "CX_mccfht_find(42) data mismatch");
    fail_if(founddata != data, "CX_mccfht_find(42) address mismatch");

    ret = CX_mccfht_del(mh);
    
    fail_if(ret != 0, "CX_mccfht_del() failed");
}
END_TEST

START_TEST(test_hashes_mhashAddOneRemove)
{
    uint32_t used = 0;
    int value = 42;
    const uint8_t *key = (const uint8_t*)&value;
    void *data = &value;
    int ret;
    CX_HashTable *mh = CX_mccfht_new(6);
    fail_if(mh == NULL, "CX_mccfht_new(6) failed");

    ret = CX_mccfht_add(mh, key, sizeof(value), data);
    fail_if(ret != 0, "CX_mccfht_add(42) failed");

    used = CX_mccfht_get_used(mh);
    fail_if(used != 1, "mhash used %lu instead of %lu", used, 1);

    ret = CX_mccfht_discard(mh, key, sizeof(value));
    fail_if(ret != 0, "CX_mccfht_discard(42) failed");

    used = CX_mccfht_get_used(mh);
    fail_if(used != 0, "mhash used %lu instead of %lu", used, 0);

    ret = CX_mccfht_del(mh);
    
    fail_if(ret != 0, "CX_mccfht_del() failed");
}
END_TEST

enum {
    MANY = 1000
};

START_TEST(test_hashes_mhashAddMany)
{
    uint32_t used = 0, size = 0, count = 0;
    int values[MANY];
    int j, ret;
    CX_HashTable *mh = CX_mccfht_new(14);
    fail_if(mh == NULL, "CX_mccfht_new(14) failed");

    for (j = 0; j < MANY; j++) {
        const uint8_t *key = (const uint8_t*)&values[j];
        void *data = &values[j];
        values[j] = j + 1;

        ret = CX_mccfht_add(mh, key, sizeof(values[j]), data);
        fail_if(ret != 0, "CX_mccfht_add(%i) failed", values[j]);
    }

    used = CX_mccfht_get_used(mh);
    fail_unless(used <= MANY, "mhash used %lu instead of %lu", used, MANY);

    size = CX_mccfht_get_size(mh);
    fail_unless(size >= MANY, "mhash size %lu instead of %lu", size, MANY);

    count = CX_mccfht_get_count(mh);
    fail_unless(count == MANY, "mhash size %lu instead of %lu", count, MANY);

    ret = CX_mccfht_del(mh);
    
    fail_if(ret != 0, "CX_mccfht_del() failed");
}
END_TEST



/*************************************************************************/

TCase *makeTestCaseHashesMhash(void)
{
    TCase *tcMhash = tcase_create("CXKit.hashes.mhash");
    tcase_add_test(tcMhash, test_hashes_mhashNew);
    tcase_add_test(tcMhash, test_hashes_mhashNewNegativePower);
    tcase_add_test(tcMhash, test_hashes_mhashNewExcessivePower);
    tcase_add_test(tcMhash, test_hashes_mhashAddOne);
    tcase_add_test(tcMhash, test_hashes_mhashAddOneGet);
    tcase_add_test(tcMhash, test_hashes_mhashAddOneRemove);
    tcase_add_test(tcMhash, test_hashes_mhashAddMany);
    return tcMhash;
}



static Suite *makeSuiteHashesMhash(void)
{
    TCase *tcMhash = makeTestCaseHashesMhash();
    Suite *s = suite_create("CXKit.hashes.mhash");
    suite_add_tcase(s, tcMhash);
    return s;
}


/*************************************************************************/

int main(int argc, char *argv[])
{
    int number_failed = 0;

    Suite *s = makeSuiteHashesMhash();
    SRunner *sr = srunner_create(s);

    srunner_run_all(sr, CK_ENV);
    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}

/*************************************************************************/

/* vim: set et sw=4 ts=4 */
/* EOF */

