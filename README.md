
cxkit - C runtime eXtension KIT
===============================

(c) 2010-2014 - Francesco Romani <fromani . gmail + com>


Overview
--------

cxkit is yet another collection of utilities for the C language.
You heard the same story over and over, but here it comes again:
cxkit is built bottom-up as a collection of modules and utilities
which I use over and over again in my open-source projects.
In the end they are consolidated in a single place, such as cxkit.

cxkit includes:
* memory allocation helpers (misallocation trace...).
* string function helpers (strip, split...).
* a minimal pluggable logging system.
* resizable array aka vector.

cxkit is intentionally organized as a set of modules as much
as possible self-contained with minimal inter-dependencies
between each other.
cxkit can be embedded in a project either as precompiled library
or jus by dropping the source code somewhere in your project tree.


License
-------

Copyright (c) 2010-2014 Francesco Romani <fromani . gmail - com>

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

    1. The origin of this software must not be misrepresented; you must not
    claim that you wrote the original software. If you use this software
    in a product, an acknowledgment in the product documentation would be
    appreciated but is not required.

    2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.

    3. This notice may not be removed or altered from any source
    distribution.

---

The authors of the code are listed as copyright holders at the beginning of
every source module.


EOF
